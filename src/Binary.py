def decimalToBinary(number):
    if (type(number).__name__ == 'str'):
        number = int(number)
    return bin(number).replace('0b', '')

def binaryToDecimal(number):
    if (type(number).__name__ == 'str' and '0b' in number):
        number = number.replace('0b', '')
    return str(int(number, 2))