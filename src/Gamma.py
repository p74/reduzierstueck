# Την κωδικοποίηση “γ”
# Προγραμματιστής: Πουιαν

import Binary

def encode(number):
    binaryNumber = Binary.decimalToBinary(number)
    code = binaryNumber
    for _ in range (len(binaryNumber) - 1):
        code = '0' + code
    return code

def decode(code):
    number = 0
    for i in range(len(code)):
        if(code[i] == '1'):
            number = Binary.binaryToDecimal(code[i:])
            break
    return number

def encoder(numberList):
    code = ''
    for number in numberList:
        code += encode(number)
    return code

def decoder(code):
    numberList = list()
    insideNumberFlag = False
    numberString = ''
    counter = 0

    for bit in code:
        if (bit == '0'):
            if not insideNumberFlag:
                counter += 1
            else:
                numberString += bit
                counter -= 1
        else:
            numberString += bit
            if not insideNumberFlag:
                insideNumberFlag = True
            else:
                counter -= 1
        if (counter <= 0 and insideNumberFlag == True):
            insideNumberFlag = False
            numberList.append(decode(numberString))
            numberString = ''
    return numberList

import random
trueCount = 0
falseCount = 0
for j in range (10000):
    limit = random.randint(1, 1000)
    numberList = list()
    for i in range(limit):
        numberList.append(str(random.randint(1, 1000)))
    #print(numberList)
    #print(decoder(encoder(numberList)))
    #print('\tTest Result #{}:'.format(j), decoder(encoder(numberList)) == numberList)
    testResult = decoder(encoder(numberList)) == numberList
    if testResult:
        trueCount += 1
    else:
        falseCount += 1

print('\nTest Results:')
print('\t' + '\x1b[6;30;42m' + ' • Passed:', str(trueCount), '\x1b[0m')
print('\t' + '\x1b[6;30;41m' + ' •Failed:', falseCount, '\x1b[0m')
print('\n')
