def encode(number):
    code = '1'
    if (type(number).__name__ == 'str'):
        number = int(number)
    for _ in range(number - 1):
        code = '0' + code
    return code

def decode(code):
    number = 0
    for _ in range(len(code)):
        number += 1
    return number

def encoder(numberList):
    code = ''
    for number in numberList:
        code += encode(number)
    return code

def decoder(code):
    numberList = list()
    number = 0
    for bit in code:
        if (bit == '0'):
            number += 1
        elif (bit == '1'):
            numberList.append(str(number+1))
            number = 0
    return numberList

print(decoder(encoder([74, 3, 2])))