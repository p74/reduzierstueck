import random
from Gamma import decoder
from Gamma import encoder

limit = random.randint(1, 10000)
numberList = list()
for i in range(limit):
    numberList.append(str(random.randint(0, 1000)))
print(numberList)
print('\n\tTest Result:', str(decoder(encoder(numberList)) == numberList))